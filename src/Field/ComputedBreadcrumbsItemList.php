<?php

namespace Drupal\computed_breadcrumbs\Field;

use Drupal\computed_breadcrumbs\Routing\BreadcrumbsResponse;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides a computed breadcrumbs field item list.
 */
class ComputedBreadcrumbsItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Symfony\Component\HttpKernel\HttpKernel $httpKernel */
    $httpKernel = \Drupal::service('http_kernel');
    /** @var \Symfony\Component\HttpFoundation\RequestStack $requestStack */
    $requestStack = \Drupal::service('request_stack');

    $parent = $this->getEntity();
    if ($parent->isNew() || !$parent->hasLinkTemplate('canonical')) {
      return;
    }
    $url = $parent->toUrl();

    $currentRequest = $requestStack->getCurrentRequest();
    $request = Request::create(
      $url->getInternalPath(),
      'GET',
      $currentRequest->query->all(),
      $currentRequest->cookies->all(),
      $currentRequest->files->all(),
      $currentRequest->server->all()
    );

    if ($currentRequest->hasSession() && $session = $currentRequest->getSession()) {
      $request->setSession($session);
    }

    $request->attributes->set('computed_breadcrumbs', TRUE);
    $response = $httpKernel->handle($request, HttpKernelInterface::SUB_REQUEST);

    // @todo Remove the request stack manipulation once the core issue described at https://www.drupal.org/node/2613044 is resolved.
    while ($requestStack->getCurrentRequest() === $request) {
      $requestStack->pop();
    }

    if ($response instanceof BreadcrumbsResponse) {
      /** @var \Drupal\Core\Menu\MenuActiveTrail $activeTrail */
      $activeTrail = \Drupal::service('menu.active_trail');
      $activeTrail->clear();
      $links = $response->getBreadcrumbs();
      $items = [];
      \Drupal::service('renderer')->executeInRenderContext(new RenderContext(), function () use ($links, &$items) {
        foreach ($links as $link) {
          $absolute = TRUE;
          if (\Drupal::config('computed_breadcrumbs.settings')->get('use_relative_urls')) {
            $absolute = FALSE;
          }
          $uri = $link->getUrl()->setAbsolute($absolute)->toString();
          if (empty($uri)) {
            $uri = 'internal:#';
          }
          $title = $link->getText();
          if (is_array($title)) {
            $title = \Drupal::service('renderer')->render($title);
          }
          $items[] = [
            'uri' => $uri,
            'title' => $title,
          ];
        }
      });

      foreach ($items as $delta => $item) {
        $this->list[] = $this->createItem($delta, $item);
      }
    }
  }

}
