<?php

namespace Drupal\computed_breadcrumbs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Computed Breadcrumbs settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'computed_breadcrumbs_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'computed_breadcrumbs.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('computed_breadcrumbs.settings');
    $form['use_relative_urls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use relative urls.'),
      '#description' => $this->t('Enable to use relative urls, otherwise absolute urls will be used.'),
      '#default_value' => $config->get('use_relative_urls'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('computed_breadcrumbs.settings');
    $config->set('use_relative_urls', $form_state->getValue('use_relative_urls'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
