<?php

namespace Drupal\computed_breadcrumbs\Plugin\Field\FieldType;

use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Breadcrumb link item.
 *
 * @FieldType(
 *   id = "computed_breadcrumbs",
 *   label = @Translation("Breadcrumbs"),
 *   description = @Translation("Breadcrumb links associated with the entity."),
 *   default_widget = "link_default",
 *   default_formatter = "link",
 *   constraints = {
 *     "LinkType" = {},
 *     "LinkAccess" = {},
 *     "LinkExternalProtocols" = {},
 *     "LinkNotExistingInternal" = {}
 *   }
 * )
 */
class ComputedBreadcrumbsLinkItem extends LinkItem {

}
