<?php

namespace Drupal\computed_breadcrumbs\Routing;

use Symfony\Component\HttpFoundation\Response;

/**
 * Kernel response containing breadcrumbs.
 */
class BreadcrumbsResponse extends Response {

  /**
   * The retrieved breadcrumbs.
   *
   * @var \Drupal\Core\Link[]
   */
  protected $breadcrumbs;

  /**
   * Set the breadcrumbs value.
   *
   * @param \Drupal\Core\Link[] $breadcrumbs
   *   The list of breadcrumbs.
   */
  public function setBreadcrumbs(array $breadcrumbs) {
    $this->breadcrumbs = $breadcrumbs;
  }

  /**
   * Retrieve the list of breadcrumb links.
   *
   * @return \Drupal\Core\Link[]
   *   The contained breadcrumbs list.
   */
  public function getBreadcrumbs() {
    return $this->breadcrumbs;
  }

}
