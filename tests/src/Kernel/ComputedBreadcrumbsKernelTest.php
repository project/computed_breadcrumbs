<?php

declare(strict_types=1);

namespace Drupal\Tests\computed_breadcrumbs\Kernel;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests node validation constraints.
 *
 * @group node
 */
class ComputedBreadcrumbsKernelTest extends EntityKernelTestBase {

  use ProphecyTrait;
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'computed_breadcrumbs',
    'link',
    'node',
    'taxonomy',
  ];

  /**
   * Set the default field storage backend for fields created during tests.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $account = $this->createUser();
    $this->container->get('current_user')->setAccount($account);

    // Create a node type for testing.
    $nodeType = NodeType::create(['type' => 'page', 'name' => 'page']);
    $nodeType->save();

    // Create a vocabulary for testing.
    $this->installEntitySchema('taxonomy_term');

    $vocabulary = Vocabulary::create(['name' => 'tags', 'vid' => 'tags']);
    $vocabulary->save();

    $breadcrumbManager = $this->prophesize('Drupal\Core\Breadcrumb\BreadcrumbManager');

    $breadcrumbManager->build(Argument::any())
      ->will(function ($args) {
        $routeMatch = $args[0];
        $breadcrumb = new Breadcrumb();
        if ($routeMatch->getRouteName() == 'entity.node.canonical') {
          $breadcrumb->addLink(new Link('Test breadcrumb', Url::fromUserInput('/breadcrumbs-test')));
        }
        if ($routeMatch->getRouteName() == 'entity.taxonomy_term.canonical') {
          $breadcrumb->addLink(new Link('Test breadcrumb', Url::fromUserInput('/breadcrumbs-test')));
        }
        return $breadcrumb;
      });
    $this->container->set('breadcrumb', $breadcrumbManager->reveal());
  }

  /**
   * Tests the computed breadcrumbs field on a node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testComputedBreadcrumbsNode() {
    $node = Node::create(['type' => 'page', 'title' => 'test', 'uid' => 1]);
    $node->save();
    $breadcrumbs = $node->get('breadcrumbs')->getValue();
    $this->assertEquals(1, count($breadcrumbs));
    $this->assertEquals('Test breadcrumb', $breadcrumbs[0]['title']);
  }

  /**
   * Tests the computed breadcrumbs field on a taxonomy term.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testComputedBreadcrumbsTerm() {
    $term = Term::create(['vid' => 'tags', 'name' => 'test term', 'tid' => 1]);
    $term->save();
    $breadcrumbs = $term->get('breadcrumbs')->getValue();
    $this->assertEquals(1, count($breadcrumbs));
    $this->assertEquals('Test breadcrumb', $breadcrumbs[0]['title']);
  }

  /**
   * Tests computed breadcrumbs in nested terms.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testNestedBreadcrumbs() {
    $parent_term = Term::create([
      'vid' => 'tags',
      'name' => 'parent term',
      'tid' => 1,
    ]);
    $parent_term->save();
    $child_term = Term::create([
      'vid' => 'tags',
      'name' => 'child term',
      'tid' => 2,
      'parent' => 1,
    ]);
    $child_term->save();

    drupal_flush_all_caches();

    $parent_term = Term::load(1);
    $child_term = Term::load(2);

    $this->assertEquals(1, count($parent_term->get('breadcrumbs')->getValue()));
    $this->assertEquals(2, count($child_term->get('breadcrumbs')->getValue()));
  }

  /**
   * Tests absolute links are used by default.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testAbsoluteLinks() {
    $term = Term::create([
      'vid' => 'tags',
      'name' => 'term',
      'tid' => 1,
    ]);
    $term->save();
    drupal_flush_all_caches();
    $term = Term::load(1);

    $this->assertStringStartsWith('http', $term->get('breadcrumbs')->getValue()[0]['uri']);
  }

  /**
   * Tests relative links are used when configured to do so.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRelativeLinks() {
    $this->config('computed_breadcrumbs.settings')->set('use_relative_urls', TRUE)->save();
    $term = Term::create([
      'vid' => 'tags',
      'name' => 'term',
      'tid' => 1,
    ]);
    $term->save();
    drupal_flush_all_caches();
    $term = Term::load(1);

    $this->assertStringStartsWith('/', $term->get('breadcrumbs')->getValue()[0]['uri']);
  }

}
