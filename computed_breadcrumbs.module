<?php

/**
 * @file
 * Defines the hooks for computed_breadcrumbs.
 */

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Implements hook_entity_base_field_info().
 */
function computed_breadcrumbs_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type instanceof ContentEntityTypeInterface) {
    $fields['breadcrumbs'] = BaseFieldDefinition::create('computed_breadcrumbs')
      ->setName('breadcrumbs')
      ->setLabel(t('Breadcrumbs'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\computed_breadcrumbs\Field\ComputedBreadcrumbsItemList')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region' => 'hidden',
        'weight' => -5,
      ]);
  }
  return $fields;
}
